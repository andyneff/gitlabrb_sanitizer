# gitlabrb_sanitizer

Sanitizes the `gitlab.rb` file by redacting and replacing sensitive config variables.
The sanitizer prints the result to stdout, no changes are made to the `gitlab.rb` file itself.

### Usage

```shell
cd /tmp
/opt/gitlab/embedded/bin/git clone --depth=1 https://gitlab.com/gitlab-com/support/toolbox/gitlabrb_sanitizer.git
cd gitlabrb_sanitizer
less sanitizer.rb # Please review before executing
/opt/gitlab/embedded/bin/ruby sanitizer.rb # --file /etc/gitlab/gitlab.rb is the default
```

Please find more options via `/opt/gitlab/embedded/bin/ruby sanitizer.rb --help`:

```shell
Usage: sanitizer.rb [options]

Specific options:
    -f, --file FILE                  Sanitize FILE (default: /etc/gitlab/gitlab.rb)
    -s, --save FILE                  Save output to FILE (default: outputs to stdout)
    -d, --sanitize-domains           Sanitize domains
    -i, --sanitize-ips               Sanitize IP addresses
    -e, --sanitize-emails            Sanitize email addresses
    -h, --help                       Show this usage message and quit.
```

or [in the `class ConfigurationParser` code](sanitizer.rb#L50).

#### Execute Directly

**Please review script before executing anything directly into sudo!**

```sh
curl -s https://gitlab.com/gitlab-com/support/toolbox/gitlabrb_sanitizer/-/raw/master/sanitizer.rb | sudo /opt/gitlab/embedded/bin/ruby
```

### Alternatives

See [our "How do I scrub sensitive information?" documentation](https://about.gitlab.com/support/sensitive-information/#how-do-i-scrub-sensitive-information).
